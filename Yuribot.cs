﻿using System.Text.Json.Serialization;

namespace Yuribot;

internal static class Yuribot
{
    public const string UserAgent = "yuribot (https://codeberg.org/starshine/yuribot)";

    public static async Task Main(string[] args)
    {
        Dotenv.Load(".env");

        var instance = GetEnvironmentVariable("INSTANCE");
        var mastodonToken = GetEnvironmentVariable("MASTODON_TOKEN");
        var danbooruUser = GetEnvironmentVariable("DANBOORU_USERNAME");
        var danbooruKey = GetEnvironmentVariable("DANBOORU_KEY");

        var danbooru = new Danbooru(danbooruUser, danbooruKey);
        var danbooruPost = await danbooru.FetchPostAsync();
        Console.WriteLine($"Fetching danbooru post {danbooruPost.Id}");

        var (image, hash) = await danbooru.FetchPostFileAsync(danbooruPost);
        var mastodonApi = new MastodonApi(instance, mastodonToken);

        var uploadId = await mastodonApi.UploadMediaAsync(
            $"yuribot-{hash}.{danbooruPost.FileExt}",
            image,
            danbooruPost.PrettyTags
        );

        Console.WriteLine($"Uploaded image to fediverse server with ID {uploadId}");

        var source =
            danbooruPost.PixivId != null
                ? $"https://www.pixiv.net/en/artworks/{danbooruPost.PixivId}"
                : danbooruPost.Source;

        await mastodonApi.PostStatusAsync(
            $"""
            Artist(s): {danbooruPost.PrettyArtists}
            Characters: {danbooruPost.PrettyCharacters}
            Media: {danbooruPost.PrettyCopyrights}

            Danbooru link: https://danbooru.donmai.us/posts/{danbooruPost.Id}
            Source: {source}
            """,
            [uploadId]
        );

        Console.WriteLine("Successfully posted status!");
    }

    private static string GetEnvironmentVariable(string envVar) =>
        Environment.GetEnvironmentVariable(envVar) ?? throw new Exception($"${envVar} is not set");
}

[JsonSerializable(typeof(MastodonApi.MastodonStatus))]
[JsonSerializable(typeof(MastodonApi.MediaAttachment))]
[JsonSerializable(typeof(Danbooru.Post[]))]
public partial class YuribotContext : JsonSerializerContext { }
