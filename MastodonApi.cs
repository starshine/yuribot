using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using J = System.Text.Json.Serialization.JsonPropertyNameAttribute;

namespace Yuribot;

public class MastodonApi(string instance, string token)
{
    private readonly HttpClient _client = new();

    /// <summary>
    /// Upload a media file to the instance.
    /// </summary>
    /// <param name="filename">The name of the file to upload.</param>
    /// <param name="file">The file to upload.</param>
    /// <param name="altText">The alt text (description) for the file.</param>
    /// <returns>The ID of the newly uploaded file.</returns>
    public async Task<string> UploadMediaAsync(string filename, Stream file, string altText)
    {
        var req = new HttpRequestMessage(HttpMethod.Post, $"{instance}/api/v2/media");
        req.Headers.Add("User-Agent", Yuribot.UserAgent);
        req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

        var content = new MultipartFormDataContent();
        content.Add(new StreamContent(file), "file", filename);
        content.Add(
            new StringContent(altText, Encoding.UTF8, MediaTypeNames.Text.Plain),
            "description"
        );

        // content.Add(new FormUrlEncodedContent([new("description", altText)]));
        req.Content = content;

        var resp = await _client.SendAsync(req);

        if (!resp.IsSuccessStatusCode)
        {
            var error = await resp.Content.ReadAsStringAsync();
            Console.WriteLine($"\nError while uploading media file:\n{error}\n");
            throw new Exception("Error when uploading media file");
        }

        var attachment = await resp.Content.ReadFromJsonAsync<MediaAttachment>(
            YuribotContext.Default.MediaAttachment
        );
        if (attachment == null)
            throw new Exception(
                "Attachment ID could not be deserialized but status code was in OK range"
            );
        return attachment.Id;
    }

    public async Task PostStatusAsync(
        string content,
        IEnumerable<string> mediaIds,
        bool sensitive = false,
        string? spoilerText = null,
        string visibility = "unlisted"
    )
    {
        var req = new HttpRequestMessage(HttpMethod.Post, $"{instance}/api/v1/statuses");
        req.Headers.Add("User-Agent", Yuribot.UserAgent);
        req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

        var ids = mediaIds.ToArray();

        Console.WriteLine(
            JsonSerializer.Serialize(
                new MastodonStatus(content, ids, sensitive, spoilerText, visibility),
                YuribotContext.Default.MastodonStatus
            )
        );

        req.Content = JsonContent.Create(
            new MastodonStatus(content, ids, sensitive, spoilerText, visibility),
            YuribotContext.Default.MastodonStatus
        );

        var resp = await _client.SendAsync(req);
        resp.EnsureSuccessStatusCode();
    }

    public record MastodonStatus(
        [property: J("status"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            string? Content,
        [property: J("media_ids"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            IEnumerable<string>? MediaIds,
        [property: J("sensitive")] bool Sensitive = false,
        [property: J("spoiler_text"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            string? SpoilerText = null,
        [property: J("visibility")] string Visibility = "unlisted"
    );

    public record MediaAttachment([property: J("id")] string Id);
}
