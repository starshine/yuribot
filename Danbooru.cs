using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using J = System.Text.Json.Serialization.JsonPropertyNameAttribute;

namespace Yuribot;

public class Danbooru(string username, string key)
{
    private readonly HttpClient _client = new();

    private const string SearchQuery = "yuri+rating:general+order:random";
    private static readonly Uri SearchUri =
        new($"https://danbooru.donmai.us/posts.json?tags={SearchQuery}&limit=1");
    private static readonly string[] BannedTags = ["meme", "nazi", "military", "war"];

    public async Task<Post> FetchPostAsync()
    {
        var attempt = 1;
        while (true)
        {
            if (attempt > 5)
                throw new Exception("FetchPostAsync went above 5 attempts, aborting fetching");

            var req = new HttpRequestMessage(HttpMethod.Get, SearchUri);
            req.Headers.Add("User-Agent", Yuribot.UserAgent);
            req.Headers.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{key}"))
            );

            var resp = await _client.SendAsync(req);
            resp.EnsureSuccessStatusCode();

            var posts = await resp.Content.ReadFromJsonAsync<Post[]>(
                YuribotContext.Default.PostArray
            );
            var post = posts?[0];

            if (post is { Valid: true })
                return post;

            attempt += 1;
        }
    }

    public async Task<(Stream Data, string Hash)> FetchPostFileAsync(Post post)
    {
        var req = new HttpRequestMessage(HttpMethod.Get, post.FileUrl);
        req.Headers.Add("User-Agent", Yuribot.UserAgent);

        var resp = await _client.SendAsync(req);
        resp.EnsureSuccessStatusCode();

        using var image = await Image.LoadAsync(await resp.Content.ReadAsStreamAsync());
        var stream = new MemoryStream(64 * 1024);
        await image.SaveAsync(stream, new JpegEncoder { Quality = 90 });

        stream.Seek(0, SeekOrigin.Begin);
        var hash = Convert.ToHexString(await SHA256.HashDataAsync(stream)).ToLower();
        stream.Seek(0, SeekOrigin.Begin);

        return (stream, hash);
    }

    public record Post(
        [property: J("id")] int Id,
        [property: J("file_ext")] string FileExt,
        [property: J("tag_string")] string TagString,
        [property: J("tag_string_artist")] string ArtistString,
        [property: J("tag_string_copyright")] string CopyrightString,
        [property: J("tag_string_character")] string CharacterString,
        [property: J("source")] string Source,
        [property: J("pixiv_id")] int? PixivId,
        [property: J("file_url")] string FileUrl,
        [property: J("score")] int Score
    )
    {
        public bool Valid =>
            Score >= 5
            && !Tags.Any(t => BannedTags.Contains(t, StringComparer.InvariantCultureIgnoreCase));

        private IEnumerable<string> Tags =>
            TagString
                .Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                .Except(Artists)
                .Except(Copyrights)
                .Except(Characters);

        private IEnumerable<string> Artists =>
            ArtistString.Split(
                ' ',
                StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries
            );

        private IEnumerable<string> Copyrights =>
            CopyrightString.Split(
                ' ',
                StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries
            );

        private IEnumerable<string> Characters =>
            CharacterString.Split(
                ' ',
                StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries
            );

        public string PrettyTags => string.Join(", ", Tags.Select(s => s.Replace('_', ' ')));
        public string PrettyCharacters =>
            string.Join(", ", Characters.Select(s => s.Replace('_', ' ')));
        public string PrettyArtists => string.Join(", ", Artists.Select(s => s.Replace('_', ' ')));
        public string PrettyCopyrights =>
            string.Join(", ", Copyrights.Select(s => s.Replace('_', ' ')));
    }
}
